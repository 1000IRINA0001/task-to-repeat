package task1;

import java.util.Scanner;

/**
 * в классе считывается строка до 1 точки;
 * выводится количество символов и пробелов до 1 точки
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите строку: ");
        String string = scan.nextLine();

        int symbol = string.indexOf(".");
        String[] symbols;
        symbols = string.split("");
        int space = 0;
        for (int i = 0; i < symbols.length; i++)
            if (symbols[i].equals(" ")) {
                space++;
            }
        System.out.println("Коллечество символов до точки: " + symbol);
        System.out.println("Колличество пробелов: " + space);


    }
}



