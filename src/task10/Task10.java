package task10;
import java.util.Scanner;
public class Task10 {

    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        System.out.println("Введите слово(фразу): ");
        String word = scanner.nextLine();
        WordPalindrom(word);
    }
    private  static void WordPalindrom(String word){
        word = word.replaceAll("[^A-Za-zА-Яа-я]", "");
        if (word.toLowerCase().equals((new StringBuffer(word)).reverse().toString().toLowerCase())) {
            System.out.println("Палиндром! " );
        } else {
            System.out.println("Не палиндром! ");
        }

    }
}

