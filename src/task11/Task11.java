package task11;
import java.util.Scanner;
public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите колличество суток : ");
        int day=scanner.nextInt();
        int dayInHours=day*24;
        int dayInMinutes=dayInHours*60;
        int dayInSeconds=dayInMinutes*60;
        System.out.println("Сутки в : ");
        System.out.println("часах = " + dayInHours );
        System.out.println("минутах = "+ dayInMinutes);
        System.out.println("секундах = "+ dayInSeconds);
    }
}
