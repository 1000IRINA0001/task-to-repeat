package task13;
import java.util.Scanner;
public class Task13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите символ: ");
        String string = scanner.nextLine();
        if(Character.isDigit(string.charAt(0))){
            System.out.println("Число");
        }
        if(Character.isLetter(string.charAt(0))){
            System.out.println("Буква");
        }
        if(".,:;".contains(string)){
            System.out.println("Знак пункутуации");
        }

    }
}
