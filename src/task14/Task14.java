package task14;

public class Task14 {
    public static void main(String[] args) {
        int[][] array = new int[4][4];

        for (int line = 0; line < array.length; line++) {
            System.out.println();
            for (int column = 0; column < array.length; column++) {
                array[line][column] = (int) (Math.random() * ((9 - 1) + 1) + 1);
                System.out.print(array[line][column] + " ");
            }
        }
        System.out.println("\n");

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                int reversedArray = array[i][j];
                array[i][j] = array[j][i];
                array[j][i] = reversedArray;
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}

