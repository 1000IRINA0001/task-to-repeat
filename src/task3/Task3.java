package task3;
import java.util.Scanner;
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        System.out.print("Введите количество рублей: ");
        double rub=scanner.nextDouble();
        System.out.println("Введите цену 1 евро: ");
        double oneEuro=scanner.nextDouble();
        System.out.printf(rub + "  рублей " + " = " +euro(rub, oneEuro)+ "  евро");
    }

    /**
     * метод переводит евро в рубли
     * @param rub кольчество рублей
     * @param oneEuro цена одного евро
     * @return возвращает количество евро
     */
    private static double euro(double rub, double oneEuro){
        double euro=rub/oneEuro;

        return euro;
    }

}
