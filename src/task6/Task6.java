package task6;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число, которое хотите умножить : ");
        int number = scanner.nextInt();
        int numbers[] = new int[11];
        for (int i = 0; i < numbers.length; i++) {
            int num = i * number;
            System.out.println(number + " * " + i + " = " + num);
        }
    }
}
