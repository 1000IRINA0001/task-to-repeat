package task7;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("введите число для проверки : ");
        double number = scanner.nextDouble();
        if (number % 1 == 0) {
            System.out.println("Число целое");
        } else {
            System.out.println("Число не целое");
        }

    }
}
